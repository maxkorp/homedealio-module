module.exports = {
  //'parser': 'babel-eslint',
  'env': {
    'es6': true,
    'node': true,
  },
  'extends': 'airbnb-base',
  'rules': {
    'arrow-body-style': 0,
    'arrow-parens': [2, 'always'],
    'brace-style': [2, 'stroustrup', {'allowSingleLine': true }],
    'comma-dangle': [2, 'never'],
  }
};
