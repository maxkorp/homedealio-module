// module.exports.Dimmer = require('./Dimmer');
module.exports.FanSpeed = require('./FanSpeed');
module.exports.Garage = require('./Garage');
module.exports.Light = require('./Light');
module.exports.Outlet = require('./Outlet');
