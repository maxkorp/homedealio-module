const fse = require('fs-extra');
const path = require('path');
const uuid = require('uuid');

const filePath = path.join(process.env.HOME, '.homedealio');

module.exports.set = (newConfig) => {
  return Promise.resolve()
    .then(() => {
      const oldConfig = module.exports.get();
      const config = {
        devices: newConfig.devices || oldConfig.devices,
        name: newConfig.name || oldConfig.name || uuid.v4(),
        wifi: newConfig.wifi || oldConfig.wifi
      };

      fse.writeJsonSync(filePath, config);

      /* eslint-disable no-console */
      console.log('wrote new config');
      console.log(config);
      console.log('Restarting');
      /* eslint-enable no-console */

      // TODO: actually restart
      return config;
    });
};

module.exports.get = () => {
  return Promise.resolve()
    .then(() => {
      return fse.readJson(filePath);
    })
    .catch(() => {
      return {};
    });
};
