const express = require('express');
const five = require('johnny-five');
const uuid = require('uuid');

const colors = [
  '#000000',
  '#FF0000',
  '#FFFF00',
  '#00FF00'
];

class FanSpeed {
  constructor(config, app) {
    this.id = config.id || uuid.v4();
    this.isFourWay = !!config.isFourWay;
    this.setSpeed(0);

    this.led = new five.Led.RGB(config.led);
    this.relays = new five.Relays(config.relays);
    this.switch = new five.Switch(config.switch);
    this.switch.on('open', () => {
      this.cycle();
    });
    this.switch.on('close', () => {
      this.cycle();
    });

    this.routerize(app);
  }

  setSpeed(speed = 0) {
    let speedNum = parseInt(speed, 10) || 0;
    speedNum = Math.max(speedNum, 0);
    speedNum = Math.min(speedNum, 3);

    this.speed = speedNum;
    this.updateLed();
    this.updateRelays();
  }

  getStatus() {
    return this.speed;
  }

  updateLed() {
    if (this.speed === 0) {
      this.led.off();
    }
    else {
      this.led.color(colors[this.speed]);
      this.led.on();
    }
  }

  updateRelays() {
    if (this.isFourWay) {
      switch (this.speed) {
        case 3:
          this.relays[0].on();
          this.relays[1].off();
          this.relays[2].off();
          break;
        case 2:
          this.relays[0].off();
          this.relays[1].on();
          this.relays[2].off();
          break;
        case 1:
          this.relays[0].off();
          this.relays[1].off();
          this.relays[2].on();
          break;
        case 0:
        default:
          this.relays[0].off();
          this.relays[1].off();
          this.relays[2].off();
          break;
      }
    }
    else {
      switch (this.speed) {
        case 3:
          this.relays[0].on();
          this.relays[1].on();
          break;
        case 2:
          this.relays[0].on();
          this.relays[1].on();
          break;
        case 1:
          this.relays[0].off();
          this.relays[1].on();
          break;
        case 0:
        default:
          this.relays[0].off();
          this.relays[1].off();
          break;
      }
    }
  }

  cycle() {
    this.setSpeed((this.speed + 1) % 4);
  }

  routerize(app) {
    this.router = express.Router();

    this.router.get('/status', (req, res) => {
      res.status(200).send(this.getStatus());
    });

    this.router.get('/off', (req, res) => {
      if (this.getStatus()) {
        this.setSpeed(0);
        res.status(200).send('OK');
      }
      else {
        res.status(400).send('Already off');
      }
    });

    this.router.get('/speed/{speed}', (req, res) => {
      if (!['0', '1', '2', '3'].includes(req.params.speed)) {
        res.status(400).send('Invalid speed');
      }
      else if (this.getStatus() === req.params.speed) {
        res.status(400).send(`Already set to ${req.params.speed}`);
      }
      else {
        this.setSpeed(req.params.speed);
        res.status(200).send('OK');
      }
    });

    this.router.get('/cycle', (req, res) => {
      this.cycle();
      res.status(200).send(this.getStatus());
    });

    app.use(`/${this.id}`, this.router);
  }
}

module.exports = FanSpeed;
