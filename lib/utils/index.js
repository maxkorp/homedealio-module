module.exports.getConfig = require('./config').get;
module.exports.setConfig = require('./config').set;
module.exports.getIp = require('./get-ip');
module.exports.setWifi = require('./set-wifi');
