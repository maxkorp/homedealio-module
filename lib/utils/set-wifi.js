const wpaSupplicant = require('wireless-tools/wpa_supplicant');

module.exports = (options) => {
  return new Promise((resolve, reject) => {
    wpaSupplicant.enable(options, (err) => {
      if (err) {
        reject(err);
      }
      else {
        resolve();
      }
    });
  });
};
