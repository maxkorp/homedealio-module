const fse = require('fs-extra');
const path = require('path');
const udev = require('udev');
const { setConfig } = require('./utils');

let monitor;

module.exports = () => {
  if (monitor) {
    return Promise.resolve(monitor);
  }

  monitor = udev.monitor();
  monitor.on('add', (device) => {
    return Promise.resolve()
      .then(() => {
        if (device.DEVTYPE !== 'disk') {
          throw new Error('invalid DEVTYPE');
        }

        return fse.readJson(path.join(device.DEVNAME, 'homedealio.config'));
      })
      .then((config) => {
        return setConfig(config);
      });
  });

  return Promise.resolve(monitor);
};
