const express = require('express');
const five = require('johnny-five');
const uuid = require('uuid');

class GarageDoor {
  constructor(config, app) {
    this.id = config.id || uuid.v4();

    this.status = false; // true === open
    this.sensor = new five.Sensor(config.sensor);
    this.sensor.on('change', () => {
      this.status = this.sensor.boolean;
    });

    this.relay = new five.Relay(config.relay);

    this.button = new five.Button(config.button);
    this.button.on('press', () => {
      this.toggle();
    });

    this.routerize(app);
  }

  getStatus() {
    return this.status;
  }

  toggle() {
    this.relay.close();
    this.relay.open();
  }

  open() {
    if (!this.getStatus()) {
      this.toggle();
    }
  }

  close() {
    if (this.getStatus()) {
      this.toggle();
    }
  }

  routerize(app) {
    this.router = express.Router();

    this.router.get('/status', (req, res) => {
      res.status(200).send(this.status ? 'OPEN' : 'CLOSED');
    });

    this.router.get('/open', (req, res) => {
      if (this.getStatus()) {
        res.status(400).send('Already open');
      }
      else {
        this.open();
        res.status(200).send('OK');
      }
    });

    this.router.get('/close', (req, res) => {
      if (this.getStatus()) {
        this.close();
        res.status(200).send('OK');
      }
      else {
        res.status(400).send('Already closed');
      }
    });

    this.router.get('/toggle', (req, res) => {
      this.toggle();
      res.status(200).send('OK');
    });

    app.use(`/${this.id}`, this.router);
  }
}

module.exports = GarageDoor;
