const express = require('express');

const devices = require('./devices');
const initBoard = require('./init-board');
const initUdev = require('./init-udev');
const { getConfig, setConfig } = require('./utils');

const app = express();

// https://github.com/nebrius/raspi-io/wiki/Pin-Information#model-abraspberry-pi-2raspberry-pi-3raspberry-pi-zero

Promise.all([initBoard(), initUdev(), getConfig()])
  .then((board, monitor, config) => {
    app.devices = config.devices.map((device) => {
      return new devices[device.type](device.config, app);
    });

    app.get('/name', (req, res) => {
      res.status(200).send(config.name);
    });

    app.get('/config', (req, res) => {
      res.status(200).send(config);
    });

    app.post('/config', (req, res) => {
      const newConfig = {}; // TODO: should probably come from request
      res.status(200).send('OK');
      setConfig(newConfig);
    });

    app.listen(process.env.port);
  });
