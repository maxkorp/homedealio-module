const express = require('express');
const five = require('johnny-five');
const uuid = require('uuid');

class Light {
  constructor(config, app) {
    this.id = config.id || uuid.v4();

    this.status = false; // true === on

    this.relay = new five.Relay(config.relay);

    this.button = new five.Button(config.button);
    this.button.on('press', () => {
      this.toggle();
    });

    this.led = new five.Led(config.led);

    this.routerize(app);
  }

  getStatus() {
    return this.relay.isOn;
  }

  toggle() {
    this.relay.toggle();
    this.led.toggle();
  }

  turnOn() {
    if (!this.getStatus()) {
      this.toggle();
    }
  }

  turnOff() {
    if (this.getStatus()) {
      this.toggle();
    }
  }

  routerize(app) {
    this.router = express.Router();

    this.router.get('/status', (req, res) => {
      res.status(200).send(this.getStatus() ? 'ON' : 'OFF');
    });

    this.router.get('/on', (req, res) => {
      if (this.getStatus()) {
        res.status(400).send('Already on');
      }
      else {
        this.open();
        res.status(200).send('OK');
      }
    });

    this.router.get('/off', (req, res) => {
      if (this.getStatus()) {
        this.close();
        res.status(200).send('OK');
      }
      else {
        res.status(400).send('Already off');
      }
    });

    this.router.get('/toggle', (req, res) => {
      this.toggle();
      res.status(200).send('OK');
    });

    app.use(`/${this.id}`, this.router);
  }
}

module.exports = Light;
