const five = require('johnny-five');
const Raspi = require('raspi-io');

let board;
module.exports = () => {
  if (board) {
    return Promise.resolve(board);
  }

  return Promise.resolve()
    .then(() => {
      board = new five.Board({
        io: new Raspi()
      });

      return new Promise((resolve) => {
        board.on('ready', () => {
          resolve(board);
        });
      });
    });
};
