const os = require('os');

module.exports = () => {
  const ifaces = os.networkInterfaces();

  return Object.keys(ifaces)
    .reduce((output, ifname) => {
      ifaces[ifname]
        .filter((iface) => iface.family === 'IPv4')
        .filter((iface) => !iface.internal)
        .forEach((iface, index) => output.push(`${ifname}:${index} ${iface.address}`));
      return output;
    }, []);
};
